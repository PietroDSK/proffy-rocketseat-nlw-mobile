import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import PageHeader from "../../components/PageHeader";
import TeacherItem, { Teacher} from "../../components/TeacherItem";
import api from "../../services/api";
import { ScrollView, TextInput, BorderlessButton, RectButton } from "react-native-gesture-handler";
import AsyncStorage from '@react-native-community/async-storage'

import { Feather } from '@expo/vector-icons'
import styles from "./styles";

function TeacherList() {

  const [isFilterVisible, setIsFiltersVisible] = useState(false);
  const [favorites, setFavorites] = useState<number[]>([])
  const [teachers, setTeachers] = useState([])

  const [subject, setSubject] = useState('');
  const [ week_day, setWeekDay] = useState('');
  const [time, setTime] = useState('');

  function loadFavorites() {
    AsyncStorage.getItem('favorites').then(response => {
      if (response) {
        const favoritedTeachers = JSON.parse(response);
        const favoritedTeachersIds = favoritedTeachers.map((teacher: Teacher) => {
          return teacher.id;
        })
        setFavorites(favoritedTeachersIds)
      }
    })
  }

  function handleToggleFiltersVisible(){
    setIsFiltersVisible(!isFilterVisible)
  }

  async function handleFiltersSubmit () {
    
    loadFavorites();
    const response = await api.get('classes', {
      params: {
        subject,
        week_day,
        time,
      }
    });
    
    console.log({favorites})
    setIsFiltersVisible(false)
    setTeachers(response.data)
  }

  return (
    <View style={styles.container}>
      <PageHeader 
      title="Proffys Disponíveis" 
      headerRight={(
        <BorderlessButton onPress={handleToggleFiltersVisible}>
          <Feather name="filter" size={20} color="#FFF"/>
        </BorderlessButton>
      )}
      
      >

        { isFilterVisible && (<View style={styles.searchForm}>
          <Text style={styles.label}> Matéria </Text>
          <TextInput 
          value={subject}
          onChangeText={text => setSubject(text)}
          placeholderTextColor= "#c1bccc"
          style={styles.input} 
          placeholder="Qual a matéria" 
          />

          <View style={styles.inputGroup}>
            <View style={styles.inputBlock}>
              <Text style={styles.label}>Dia da semana</Text>
              <TextInput 
              value={week_day}
              onChangeText={text => setWeekDay(text)}
              placeholderTextColor= "#c1bccc"
              style={styles.input} 
              placeholder="Qual o dia?" 
              />
            </View>

            <View style={styles.inputBlock}>
              <Text style={styles.label}>Horário</Text>
              <TextInput 
              value={time}
              onChangeText={text => setTime(text)}
              placeholderTextColor= "#c1bccc"
              style={styles.input} 
              placeholder="Qual horario?" 
              />
            </View>
          </View>

          <RectButton onPress={handleFiltersSubmit} style={styles.submitButton}>
            <Text style={styles.submitButtonText}>Filtrar</Text>
          </RectButton>
        </View>
        )}
      </PageHeader>

      <ScrollView
        style={styles.teacherList}
        contentContainerStyle={{
          paddingHorizontal: 16,
          paddingBottom: 16,
        }}
      >
        {teachers.map((teacher: Teacher)=> {
        return (
        <TeacherItem 
        key={teacher.id} 
        teacher={teacher}
        favorited={favorites.includes(teacher.id)}
        />
)
        })}

      </ScrollView>
    </View>
  );
}

export default TeacherList;
